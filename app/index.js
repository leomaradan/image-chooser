const path = require('path');
const { app, BrowserWindow, ipcMain, dialog } = require('electron');
const fs = require('fs');

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
  app.quit();
}

const isDev = process.env.IS_DEV === 'true';

const createWindow = () => {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
    },
  });

  // Open the DevTools.
  if (isDev) {
    mainWindow.loadURL('http://localhost:5173');
    mainWindow.webContents.openDevTools();
  } else {
    mainWindow.removeMenu();
    mainWindow.loadFile(path.join(__dirname, 'build', 'index.html'));
  }
};

app.whenReady().then(() => {
  createWindow();

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit();
});

ipcMain.handle('openImageDialog', () => {
  return dialog
    .showOpenDialog({
      title: 'Select images',
      properties: [
        'openDirectory',
        //'multiSelections',
        //'showHiddenFiles',
        //'dontAddToRecent',
      ],
      // filters: [{ name: 'Images', extensions: ['jpg', 'png', 'gif'] }],
    })
    .then((files) => {
      //return files.filePaths;

      /*files.filePaths.forEach((image) => {
        const imageContent = fs.readFileSync(image).toString('base64');
        console.log({ imageContent });
        result[image] = imageContent;
      });
      console.log({ files, result });
      return result;*/
      if (files.filePaths.length === 0) {
        throw new Error('invalid path');
      }
      return files.filePaths.find((file) => file);
    })
    .then((sourcePath) => {
      // const images = {};
      const images = [];
      const files = fs.readdirSync(sourcePath, { encoding: 'utf8' });

      files.forEach((file) => {
        const fullPath = path.join(sourcePath, file);

        if (/(\.png$|\.jpe?g$)/.test(fullPath)) {
          images.push(fullPath);
        }
        //const imageContent = fs.readFileSync(fullPath).toString('base64');
      });

      return images;
    });
});

ipcMain.handle('getImages', (_event, image) => {
  const data = fs.readFileSync(image).toString('base64');

  return data;
});
//filterResult
ipcMain.handle('filterResult', (_event, results) => {
  try {
    const baseDir = path.dirname(results.selected[0]);
    // create directories
    const selectedPath = path.resolve(baseDir, 'selected');
    const rejectedPath = path.resolve(baseDir, 'rejected');

    fs.mkdirSync(selectedPath);
    fs.mkdirSync(rejectedPath);

    results.selected.forEach(selected => {
      const filename = path.basename(selected);
      fs.copyFileSync(selected, path.join(selectedPath, filename));
      fs.unlinkSync(selected);
    });

    results.rejected.forEach(rejected => {
      const filename = path.basename(rejected);
      fs.copyFileSync(rejected, path.join(rejectedPath, filename));
      fs.unlinkSync(rejected);
    });
    
  } catch(e) {
    if(e.message !== undefined) {
      return e.message
    }
    
    return 'error while writing files';
  }

});