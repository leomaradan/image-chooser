const { contextBridge, ipcRenderer } = require('electron');

window.addEventListener('DOMContentLoaded', () => {
  console.log('Preload script called');
});

contextBridge.exposeInMainWorld('api', {
  openImageDialog: async () => {
    try {
      return await ipcRenderer.invoke('openImageDialog');
    } catch (e) {
      return console.log(e);
    }
  },
  getImages: async (image) => {
    console.log('image', image);
    try {
      return await ipcRenderer.invoke('getImages', image);
    } catch (e) {
      return console.log(e);
    }
  },
  filterResult: async (results) => {
    try {
      return await ipcRenderer.invoke('filterResult', results);
    } catch (e) {
      return console.log(e);
    }
  },
});
