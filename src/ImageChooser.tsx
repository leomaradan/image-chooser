import React, { useEffect } from 'react';
import ABChoose from './ABChoose';
import { useAppDispatch } from './features/hooks';
import {
  useSelectSource,
  useChooserImages,
  startChoosing,
} from './features/images';
import Step, { Steps } from './Step';

const ImageChooser = () => {
  const images = useSelectSource();

  const dispatch = useAppDispatch();

  const [imageA, imageB] = useChooserImages();

  useEffect(() => {
    dispatch(startChoosing());
  }, [dispatch, images]);

  if (!imageA || !imageB) {
    return null;
  }

  return (
    <>
      <Step activeStep={Steps.Chooser} />
      <ABChoose />
    </>
  );
};

export default React.memo(ImageChooser);
