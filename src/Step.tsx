import Stepper from '@mui/material/Stepper';
import MUIStep from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import React, { FC, useCallback } from 'react';
import StepButton from '@mui/material/StepButton';
import { useAppDispatch } from './features/hooks';
import { restart } from './features/images';

export enum Steps {
  Configuration = 0,
  Chooser = 1,
  Result = 2,
}

export interface StepProps {
  activeStep: Steps;
}
const Step: FC<StepProps> = ({ activeStep }) => {
  const dispatch = useAppDispatch();
  const handleRestartConfig = useCallback(() => {
    dispatch(restart('configure'));
  }, [dispatch]);

  const handleRestartChoose = useCallback(() => {
    dispatch(restart('choose'));
  }, [dispatch]);

  return (
    <Stepper activeStep={activeStep} sx={{ height: '10vh' }}>
      <MUIStep>
        <StepButton onClick={handleRestartConfig}>Select images</StepButton>
      </MUIStep>
      <MUIStep>
        <StepButton onClick={handleRestartChoose}>Choosing process</StepButton>
      </MUIStep>
      <MUIStep>
        <StepLabel>Final result</StepLabel>
      </MUIStep>
    </Stepper>
  );
};

export default React.memo(Step);
