import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ImageListItemBar from '@mui/material/ImageListItemBar';
import Stack from '@mui/material/Stack';

import React, { useCallback, useState } from 'react';
import { useSelectSource, useSelectSelected } from './features/images';
import Image from './Image';
import Step, { Steps } from './Step';

const ResultList = () => {
  const allImages = useSelectSource();
  const selected = useSelectSelected();

  const [error, setError] = useState<string | undefined>();
  const [disabledWrite, setDisabledWrite] = useState(false);

  const writeResult = useCallback(() => {
    setDisabledWrite(true);
    const rejected: string[] = [];
    allImages.forEach((key) => {
      if (!selected.find((select) => select === key)) {
        rejected.push(key);
      }
    });
    api.filterResult({ rejected, selected }).then((result) => {
      setError(result);
    });
  }, [allImages, selected]);

  return (
    <>
      <Step activeStep={Steps.Result} />
      <Stack>
        <Button
          variant="contained"
          onClick={writeResult}
          disabled={disabledWrite && error === undefined}
        >
          Write result
        </Button>
        {error && <Alert severity="error">{error}</Alert>}
        <ImageList>
          {selected.map((item) => (
            <ImageListItem key={item}>
              <Image source={item} />
              <ImageListItemBar title={item} />
            </ImageListItem>
          ))}
        </ImageList>
      </Stack>
    </>
  );
};

export default React.memo(ResultList);
