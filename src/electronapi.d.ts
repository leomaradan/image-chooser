declare const api: {
  // openImageDialog: () => Promise<Record<string, string>>;
  openImageDialog: () => Promise<string[]>;
  getImages: (images: string) => Promise<string>;
  filterResult: (results: {
    selected: string[];
    rejected: string[];
  }) => Promise<undefined | string>;
};
