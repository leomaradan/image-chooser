import React from 'react';
import ImageChooser from './ImageChooser';
import Configure from './Configure';
import ResultList from './ResultList';
import { useCurrentState } from './features/images';

const Home = () => {
  const state = useCurrentState();

  if (state === 'configure') {
    return <Configure />;
  }

  if (state === 'result') {
    return <ResultList />;
  }

  return <ImageChooser />;
};

export default React.memo(Home);
