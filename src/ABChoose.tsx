import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import { useTheme } from '@mui/material/styles';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import LinearProgress from '@mui/material/LinearProgress';
import Typography from '@mui/material/Typography';
import Image from './Image';
import { useAppDispatch, useAppSelector } from './features/hooks';
import { selectionDone, useChooserImages } from './features/images';

const normalise = (value: number, max: number) => (value * 100) / max;

const ABChoose = () => {
  const [selected, setSelected] = useState<'a' | 'b' | 'a+b' | 'nothing'>(
    'nothing'
  );

  const theme = useTheme();

  const dispatch = useAppDispatch();
  const { pass, tempRejected, tempSelected, tempSerie } = useAppSelector(
    (state) => state.images.chooser
  );

  const nbWanted = useAppSelector((state) => state.images.nbWanted);

  const [imageA, imageB] = useChooserImages();

  useEffect(() => {
    setSelected('nothing');
  }, [imageA, imageB]);

  const clickImageA = useCallback(() => {
    switch (selected) {
      case 'a':
        setSelected('nothing');
        break;
      case 'b':
        setSelected('a+b');
        break;
      case 'a+b':
        setSelected('b');
        break;
      case 'nothing':
      default:
        setSelected('a');
    }
  }, [selected]);

  const clickImageB = useCallback(() => {
    switch (selected) {
      case 'a':
        setSelected('a+b');
        break;
      case 'b':
        setSelected('nothing');
        break;
      case 'a+b':
        setSelected('a');
        break;
      case 'nothing':
      default:
        setSelected('b');
    }
  }, [selected]);

  const validateAndFinish = useCallback(() => {
    dispatch(selectionDone({ selection: selected, finish: true }));
  }, [dispatch, selected]);

  const validate = useCallback(() => {
    dispatch(selectionDone({ selection: selected }));
  }, [dispatch, selected]);

  const current = useMemo(
    () => tempSelected.length + tempRejected.length,
    [tempRejected.length, tempSelected.length]
  );
  const max = useMemo(
    () => tempSerie.length + current,
    [current, tempSerie.length]
  );

  const selectedA = useMemo(
    () => selected === 'a' || selected === 'a+b',
    [selected]
  );
  const selectedB = useMemo(
    () => selected === 'b' || selected === 'a+b',
    [selected]
  );

  const lastStep = useMemo(() => current + 2 >= max, [current, max]);

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Box sx={{ display: 'flex', alignItems: 'center', gap: 2 }}>
          <Box>
            <Typography variant="body2" color="text.secondary">
              Selected : {tempSelected.length} / {nbWanted}
            </Typography>
          </Box>
          <Box sx={{ flex: 1 }}>
            <LinearProgress
              value={normalise(current, max)}
              variant="determinate"
            />
          </Box>
          <Box>
            <Typography variant="body2" color="text.secondary">
              Pass : {pass}
            </Typography>
          </Box>
        </Box>
      </Grid>
      <Grid item xs={6}>
        <Stack
          direction="row"
          sx={{
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Image
            style={{
              border: `3px solid ${
                selectedA ? theme.palette.primary.main : 'black'
              }`,
            }}
            source={imageA}
            onClick={clickImageA}
          />
        </Stack>
      </Grid>
      <Grid item xs={6}>
        <Stack
          direction="row"
          sx={{
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Image
            style={{
              border: `3px solid ${
                selectedB ? theme.palette.primary.main : 'black'
              }`,
            }}
            source={imageB[0]}
            onClick={clickImageB}
          />
        </Stack>
      </Grid>
      <Grid item xs={4} />
      <Grid item xs={4}>
        <Button variant="contained" onClick={validate}>
          Validate {selected}
        </Button>
      </Grid>
      <Grid item xs={4}>
        <Button
          variant="contained"
          onClick={validateAndFinish}
          disabled={!lastStep}
        >
          Validate {selected} + finish selection
        </Button>
      </Grid>
    </Grid>
  );
};

export default React.memo(ABChoose);
