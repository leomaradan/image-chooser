/* eslint-disable no-param-reassign */
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { useAppSelector } from './hooks';

function shuffle<T>(a: T[]): T[] {
  let j;
  let x;
  let i;
  const newA = [...a];
  for (i = newA.length - 1; i > 0; i -= 1) {
    j = Math.floor(Math.random() * (i + 1));
    x = a[i];
    newA[i] = a[j];
    newA[j] = x;
  }
  return newA;
}

// Define a type for the slice state
interface ImagesSlice {
  sourceImages: string[];
  nbWanted: number;
  selected: string[];
  chooser: {
    tempSerie: string[];
    tempRejected: string[];
    tempSelected: string[];
    imageA?: string;
    imageB?: [string, boolean];
    pass: number;
  };
}

// Define the initial state using that type
const initialChooserState: ImagesSlice['chooser'] = {
  tempSerie: [],
  tempRejected: [],
  tempSelected: [],
  pass: 1,
};

const initialState: ImagesSlice = {
  sourceImages: [],
  nbWanted: 0,
  selected: [],
  chooser: initialChooserState,
};

const selectImages = (state: ImagesSlice) => {
  const shuffled = shuffle([...state.chooser.tempSerie]);
  const shuffledRejected = shuffle([...state.chooser.tempRejected]);

  [state.chooser.imageA] = shuffled;
  let imageBName = '';

  let valid = true;

  if (shuffled.length > 1) {
    [, imageBName] = shuffled;
  } else {
    [imageBName] = shuffledRejected;

    valid = false;
  }

  state.chooser.imageB = [imageBName, valid];
  return state;
};
const chooserGetImages = (state: ImagesSlice, finish?: boolean) => {
  if (state.chooser.tempSerie.length > 0 && !finish) {
    return selectImages(state);
  }

  const selectedCount = state.chooser.tempSelected.length;
  if (finish || selectedCount <= state.nbWanted) {
    state.selected = state.chooser.tempSelected;
    state.chooser = initialChooserState;

    return state;
  } /* else if (selectedCount < nbWanted) {
          setSerie(rejected);
          //setSelected({});
          setRejected({});
          setFinalSelection(true);
          setPass((current) => current + 1);
        } */

  state.chooser.tempSerie = state.chooser.tempSelected;
  state.chooser.tempSelected = [];
  state.chooser.tempRejected = [];
  state.chooser.pass += 1;

  return selectImages(state);
};

/* eslint-disable no-param-reassign */
export const imagesSlice = createSlice({
  name: 'images',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    configure: (state, action: PayloadAction<[number, string[]]>) => {
      [state.nbWanted, state.sourceImages] = action.payload;
    },
    restart: (state, action: PayloadAction<'configure' | 'choose'>) => {
      if (action.payload === 'choose') {
        state.selected = [];
      } else {
        state.nbWanted = 0;
        state.selected = [];
        state.sourceImages = [];
      }
    },
    startChoosing: (state) => {
      state.chooser.tempSerie = state.sourceImages;
      return chooserGetImages(state);
    },
    selectionDone: (
      state,
      action: PayloadAction<{
        selection: 'a' | 'b' | 'a+b' | 'nothing';
        finish?: boolean;
      }>
    ) => {
      const imageAName = state.chooser.imageA as string;

      const imageBName = (state.chooser.imageB as [string, boolean])[0];
      const imageBValid = (state.chooser.imageB as [string, boolean])[1];
      switch (action.payload.selection) {
        case 'a':
          state.chooser.tempSelected.push(imageAName);

          if (imageBValid) {
            state.chooser.tempRejected.push(imageBName);
          }

          break;
        case 'b':
          state.chooser.tempRejected.push(imageAName);

          if (imageBValid) {
            state.chooser.tempSelected.push(imageBName);
          }
          break;
        case 'a+b':
          state.chooser.tempSelected.push(imageAName);
          if (imageBValid) {
            state.chooser.tempSelected.push(imageBName);
          }
          break;
        case 'nothing':
        default:
          state.chooser.tempRejected.push(imageAName);

          if (imageBValid) {
            state.chooser.tempRejected.push(imageBName);
          }
          break;
      }

      const newSerie: string[] = [];
      state.chooser.tempSerie.forEach((key) => {
        if (key !== imageAName && key !== imageBName) {
          newSerie.push(key);
        }
      });

      state.chooser.tempSerie = newSerie;
      return chooserGetImages(state, action.payload.finish);
    },
  },
});

export const { configure, restart, selectionDone, startChoosing } =
  imagesSlice.actions;

export const useSelectSource = () =>
  useAppSelector((state) => state.images.sourceImages);
export const useSelectNbWanted = () =>
  useAppSelector((state: RootState) => state.images.nbWanted);
export const useSelectSelected = () =>
  useAppSelector((state: RootState) => state.images.selected);

export const useCurrentState = () =>
  useAppSelector((state: RootState) => {
    if (state.images.sourceImages.length === 0 || state.images.nbWanted === 0) {
      return 'configure';
    }

    if (state.images.selected.length !== 0) {
      return 'result';
    }

    return 'choose';
  });

export const useChooserImages = (): [string, [string, boolean]] =>
  useAppSelector((state: RootState) => [
    state.images.chooser.imageA as string,
    state.images.chooser.imageB as [string, boolean],
  ]);

export default imagesSlice.reducer;
