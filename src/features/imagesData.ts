// Need to use the React-specific entry point to import createApi
import { BaseQueryFn, createApi } from '@reduxjs/toolkit/query/react';

export interface CounterState {
  value: number;
}

const axiosBaseQuery =
  (): BaseQueryFn<
    {
      key: string;
    },
    string,
    unknown
  > =>
  async ({ key }) => {
    const result = await api.getImages(key);
    return { data: result };
  };

export const imagesData = createApi({
  reducerPath: 'imagesData',
  baseQuery: axiosBaseQuery(),
  endpoints(build) {
    return {
      image: build.query({ query: (key: string) => ({ key }) }),
    };
  },
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useImageQuery } = imagesData;
