import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import React, { useCallback, useMemo, useState } from 'react';
import Step, { Steps } from './Step';
import { useAppDispatch } from './features/hooks';
import { configure } from './features/images';

const Configure = () => {
  const [images, setImages] = useState<string[]>([]);
  const [nbWanted, setNbWanted] = useState<number>(0);

  const dispatch = useAppDispatch();

  const ready = useMemo(
    () => nbWanted !== 0 && images.length > nbWanted,
    [images.length, nbWanted]
  );

  const openFileDialog = useCallback(async () => {
    const result = await api.openImageDialog();

    if (result !== undefined) {
      setImages(result);
    }
  }, []);

  const start = useCallback(() => {
    if (images !== null && nbWanted !== null) {
      dispatch(configure([nbWanted, images]));
    }
  }, [dispatch, images, nbWanted]);

  return (
    <>
      <Step activeStep={Steps.Configuration} />
      <Container maxWidth="sm">
        <Stack spacing={2} direction="column">
          <Button variant="contained" onClick={openFileDialog}>
            Select images
          </Button>
          {images.length > 0 && (
            <Box>{Object.keys(images).length} image(s) selected</Box>
          )}
          <TextField
            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
            value={nbWanted}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setNbWanted(Number(event.target.value));
            }}
          />
          <Button variant="contained" disabled={!ready} onClick={start}>
            Start
          </Button>
        </Stack>
      </Container>
    </>
  );
};

export default React.memo(Configure);
