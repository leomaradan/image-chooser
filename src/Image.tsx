/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { CSSProperties, FC } from 'react';
import { useImageQuery } from './features/imagesData';

export interface ImageProps {
  source: string;
  style?: CSSProperties;
  onClick?: () => void;
}

const Image: FC<ImageProps> = ({ source, onClick, style }) => {
  const { data, error, isLoading } = useImageQuery(source);

  if (error || isLoading) {
    return null;
  }

  return (
    <img
      style={{
        maxWidth: '100%',

        maxHeight: '80vh',
        ...style,
      }}
      src={`data:image/png;base64,${data}`}
      onClick={onClick}
      onKeyDown={onClick}
      alt="Comparing preview"
      loading="lazy"
    />
  );
};

export default React.memo(Image);
